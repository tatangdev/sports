<!-- START: ASIDE MENU -->
<section class="">
    <div class="border-t border-b border-gray-200 py-12 mt-16 px-4">

<!-- This example requires Tailwind CSS v2.0+ -->
<div class="bg-white">
    <div class="max-w-7xl mx-auto py-12 px-4 text-center sm:px-6 lg:px-8 lg:py-24">
      <div class="space-y-12">
        <div class="space-y-5 sm:mx-auto sm:max-w-xl sm:space-y-4 lg:max-w-5xl">
          <h3 class="text-2xl capitalize font-semibold">
            Meet our team
          </h3>
          <p class="text-l text-gray-500">Our team members</p>
        </div>
        <ul role="list" class="mx-auto space-y-16 sm:grid sm:grid-cols-2 sm:gap-16 sm:space-y-0 lg:grid-cols-3 lg:max-w-5xl">
          <li>
            <div class="space-y-6">
              <img class="mx-auto h-40 w-40 rounded-full xl:w-56 xl:h-56" src="./frontend/images/content/member-wahyudi.jpeg" alt="">
              <div class="space-y-2">
                <div class="text-lg leading-6 font-medium space-y-1">
                  <h3>Wahyudi</h3>
                  <p class="text-indigo-600">190210115</p>
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="space-y-6">
              <img class="mx-auto h-40 w-40 rounded-full xl:w-56 xl:h-56" src="./frontend/images/content/member-dewa.jpeg" alt="">
              <div class="space-y-2">
                <div class="text-lg leading-6 font-medium space-y-1">
                  <h3>Dewa kurniawan</h3>
                  <p class="text-indigo-600">190210117</p>
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="space-y-6">
              <img class="mx-auto h-40 w-40 rounded-full xl:w-56 xl:h-56" src="./frontend/images/content/member-fitri.jpeg" alt="">
              <div class="space-y-2">
                <div class="text-lg leading-6 font-medium space-y-1">
                  <h3>Fitri natalia</h3>
                  <p class="text-indigo-600">190210034</p>
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="space-y-6">
              <img class="mx-auto h-40 w-40 rounded-full xl:w-56 xl:h-56" src="./frontend/images/content/member-veronika.jpeg" alt="">
              <div class="space-y-2">
                <div class="text-lg leading-6 font-medium space-y-1">
                  <h3>Juwita veronika</h3>
                  <p class="text-indigo-600">190210128</p>
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="space-y-6">
              <img class="mx-auto h-40 w-40 rounded-full xl:w-56 xl:h-56" src="./frontend/images/content/member-rike.jpeg" alt="">
              <div class="space-y-2">
                <div class="text-lg leading-6 font-medium space-y-1">
                  <h3>Brantaricke oktorija</h3>
                  <p class="text-indigo-600">190210023</p>
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="space-y-6">
              <img class="mx-auto h-40 w-40 rounded-full xl:w-56 xl:h-56" src="./frontend/images/content/member-otoritas.jpeg" alt="">
              <div class="space-y-2">
                <div class="text-lg leading-6 font-medium space-y-1">
                  <h3>Otoritas bawaulu</h3>
                  <p class="text-indigo-600">190210085</p>
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="space-y-6">
              <img class="mx-auto h-40 w-40 rounded-full xl:w-56 xl:h-56" src="./frontend/images/content/member-muklis.jpeg" alt="">
              <div class="space-y-2">
                <div class="text-lg leading-6 font-medium space-y-1">
                  <h3>Muklis Ansori</h3>
                  <p class="text-indigo-600">190210037</p>
                </div>
              </div>
            </div>
          </li>

          <!-- More people... -->
        </ul>
      </div>
    </div>
  </div>
    </div>
</section>
<!-- END: ASIDE MENU -->

<!-- START: FOOTER -->
<footer class="flex text-center px-4 py-8 justify-center">
    <p class="text-sm">
    Copyright 2022 • All Rights Reserved olSports
    </p>
</footer>
<!-- END: FOOTER -->
